import { Component, OnInit, Output, EventEmitter, NgZone, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';

import { LoggerService } from '../../core/logger.service';
import { GeolocationService } from '../geolocation.service';
import { MatAutocomplete } from '@angular/material';
import { FavouritePlacesService } from '../favourite-places.service';
import { GooglePlaceService } from '../google-place.service';

declare var google: any;

@Component({
    selector: 'app-nearby-search-bar',
    templateUrl: './search-bar.component.html'
})
export class SearchBarComponent implements OnInit {

    @Output() results = new EventEmitter();
    @Output() showFavourites = new EventEmitter();

    searchSubject: Subject<string> = new Subject();

    sortTypes: any[] = [{ key: 'DISTANCE', value: 'distance' }, { key: 'PROMINENCE', value: 'popularity' }];
    selectedSortType: string = 'PROMINENCE';

    placeTypes: any[] = [{ key: 'cafe', value: 'cafe' }, { key: 'restaurant', value: 'restaurant' }, { key: 'art_gallery', value: 'art gallery' }];
    selectedType: any;

    currentLocation: any = {
        lat: 40.7828647,
        lng: -73.9653551
    };

    searchText: any;
    resultList: any;

    constructor(private favouritePlacesService: FavouritePlacesService, private loggerService: LoggerService,
        private googlePlaceService: GooglePlaceService, private geoLocatlionService: GeolocationService,
        private ngZone: NgZone) {

    }

    ngOnInit() {
        this.geoLocatlionService.getLocation({}).subscribe(
            data => {
                this.loggerService.log('Location found: ' + data);
                this.currentLocation = {
                    lat: data.coords.latitude,
                    lng: data.coords.longitude
                };
            }, error => {
                this.loggerService.error(error);
            }
        );
        this.searchSubject.pipe(debounceTime(200)).subscribe((event: any) => {
            this.placeSearch();
        });
        this.selectedSortType = 'PROMINENCE';
    }

    placeSearch() {
        if (!this.searchText || this.searchText.length === 0) {
            this.resultList = [];
            this.results.emit([]);
            return;
        }

        this.googlePlaceService.placeSearch(this.searchText, this.currentLocation, this.selectedSortType, this.selectedType, this.callback.bind(this));
    }

    callback(results) {
        this.ngZone.run(() => {
            let __this = this;
            setTimeout(() => {
                __this.resultList = results;
                __this.results.emit(results);
            }, 100);
        });
    }

    onShowFavourites() {
        this.resultList = this.favouritePlacesService.getFavouritePlaces();
        this.results.emit(this.resultList);
    }
}
