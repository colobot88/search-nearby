import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NearbyComponent } from './nearby.component';
import { GeolocationService } from './geolocation.service';
import { SharedModule } from '../shared/shared.module';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { GooglePlaceService } from './google-place.service';
import { SearchResultsComponent } from './search-results/search-results.component';
import {Ng2Webstorage} from 'ngx-webstorage';
import { FavouritePlacesService } from './favourite-places.service';
import { PlaceCardComponent } from './place-card/place-card.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        Ng2Webstorage
    ],
    declarations: [
        NearbyComponent,
        SearchBarComponent,
        SearchResultsComponent,
        PlaceCardComponent,
    ],
    providers: [
        GeolocationService,
        GooglePlaceService,
        FavouritePlacesService
    ],
    exports: [
        CommonModule,
        FormsModule,
        NearbyComponent
    ]
})
export class NearbyModule { }