import {Injectable, OnInit} from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';

@Injectable()
export class FavouritePlacesService implements OnInit {

    private favouritePlaces: any[];

    constructor(private localSt: LocalStorageService) {
        this.favouritePlaces = JSON.parse(this.localSt.retrieve('favouritePlaces'));
        if (!this.favouritePlaces) {
            this.favouritePlaces = [];
        }
    }

    ngOnInit() {
    }

    getFavouritePlaces() {
        return this.favouritePlaces;
    }

    addFavouritePlace(place: any) {
        if (!this.isFavourite(place.place_id)) {
            this.favouritePlaces.push(place);
            this.localSt.store('favouritePlaces', JSON.stringify(this.favouritePlaces));
        }
    }

    removeFavouritePlace(place: any) {
        if (this.isFavourite(place.place_id)) {
            this.favouritePlaces.forEach((_place, index) => {
                if (_place.place_id === place.place_id) {
                    this.favouritePlaces.splice(index, 1);
                }
            });
            this.localSt.store('favouritePlaces', JSON.stringify(this.favouritePlaces));
        }
    }

    isFavourite(id) {
        let favourite = false;
        if (!this.favouritePlaces) {
            favourite = false;
        }
        this.favouritePlaces.forEach(place => {
            if (place.place_id === id) {
                favourite = true;
            }
        });
        return favourite;
    }
}
