import { Component, OnInit } from '@angular/core';
import { GeolocationService } from './geolocation.service';
import { LoggerService } from '../core/logger.service';

declare var google: any;

@Component({
    selector: 'app-nearby',
    templateUrl: './nearby.component.html',
    styleUrls: ['./nearby.component.css']
})
export class NearbyComponent {

    searchResults: any;

    constructor() {

    }

}
