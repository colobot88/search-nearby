import { Component, OnInit, Output, EventEmitter, Input, NgZone } from '@angular/core';
import { GooglePlaceService } from '../google-place.service';

@Component({
    selector: 'app-nearby-place-card',
    templateUrl: './place-card.component.html',
    styleUrls: ['./place-card.component.css']
})
export class PlaceCardComponent {

    @Input() isFavourite: any;
    @Input() place: any;
    @Output() favourite = new EventEmitter();

    details: any;

    constructor(private googlePlaceService: GooglePlaceService, private ngZone: NgZone) {

    }

    favouriteChanged(favourite: boolean) {
        this.favourite.emit(favourite);
    }

    placeDetails(placeid: any) {
        this.googlePlaceService.placeDetails(placeid, this.detailsCallback.bind(this));
    }

    detailsCallback(place) {
        this.ngZone.run(() => {
            let __this = this;
            setTimeout(() => {
                __this.details = place;
            }, 100);
        });
    }
}
