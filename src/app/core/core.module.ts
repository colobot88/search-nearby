import { NgModule, SkipSelf, Optional } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

import { LoggerService } from './logger.service';
import { throwIfAlreadyLoaded } from './module-import-guard';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule, // we use ngFor
    BrowserAnimationsModule,
    HttpClientModule,
    FlexLayoutModule
  ],
  exports: [],
  declarations: [],
  providers: [LoggerService]
})
export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }
}
