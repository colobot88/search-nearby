import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FavouritePlacesService } from '../favourite-places.service';

@Component({
    selector: 'app-nearby-search-results',
    templateUrl: './search-results.component.html',
    styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent {

    @Input() searchResults: any;

    constructor(private favouritePlacesService: FavouritePlacesService) {

    }

    updateFavouritePlace(place: any, favourite: boolean = true) {
        if (favourite) {
            this.favouritePlacesService.addFavouritePlace(place);
        } else {
            this.favouritePlacesService.removeFavouritePlace(place);
        }
    }

    isFavourite(id: any) {
        return this.favouritePlacesService.isFavourite(id);
    }
}
