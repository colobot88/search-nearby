import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';

declare var google: any;

@Injectable()
export class GooglePlaceService {

    constructor(private ngZone: NgZone) { }

    placeSearch(searchText: string, location: any, sortType: string, type: string, callback: any) {
        let request = {
            keyword: searchText,
            location: location,
            radius: sortType !== 'DISTANCE' ? 50000 : '',
            type: [type],
            rankBy: sortType === 'DISTANCE' ? google.maps.places.RankBy.DISTANCE : google.maps.places.RankBy.PROMINENCE
        };

        this.ngZone.run(() => {

            let service = new google.maps.places.PlacesService(document.createElement('div'));
            service.nearbySearch(request, function (results, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    callback(results);
                }
            });
        });
    }

    placeDetails(placeid: any, callback: any) {
        this.ngZone.run(() => {
            let service = new google.maps.places.PlacesService(document.createElement('div'));
            service.getDetails({
                placeId: placeid
            }, function (place, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    callback(place);
                }
            });
        });
    }

}
